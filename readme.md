Symfony 5 et vuejs fullstack

1- installation

git clone  https://gitlab.com/Djehya/food.git

2 cd food

3 composer install  & npm install

4 php bin/console doctrine:database:create

5 php bin/console make:migration

6 php bin/console doctrine:fixtures:load

7 symfony serv -d --no --tls

8 npm run dev

Url de test sur le postman

Method GET
Menu liste par category
http://127.0.0.1:8000/api/listArticle

Method POST
Ajouter article au panier
params id article
http://127.0.0.1:8000/api/article/add

Method PUT
Voir detail article par category next prev
params 1  id_category exemple et article_slug
http://127.0.0.1:8000/show/1/cucumber-salad

Method delete
reinitialise cart session
http://127.0.0.1:8000/api/cart/rest

Method GET
reservation  par user
params user
http://127.0.0.1:8000/api/profile/allReservation

Method POST
accepter ou refuser reservation
params id_reservation ,
status [false] , [true]
http://127.0.0.1:8000/api/admin/reservation/accept

Methog GET
Tous les sessions cart enregistré
http://127.0.0.1:8000/api/article/viewListCartArticle

Method POST
Supprimer tous les sessions article
params id article
http://127.0.0.1:8000/api/article/remove

Method POST 
Ajouter ou Supprime un article
params id
params status boolean 
[true] = supprimer  
[false] = ajouter 
http://127.0.0.1:8000/api/article/add

Method POST
Ajouter reservation
params date , time , person
http://127.0.0.1:8000/api/reservation/add
Method POST
valider mon panier

query params sur postman
email ,
password,
lastname ,
address , 
phone , 
firstname ,
id ,
ID reservation dans la base de donnee
http://127.0.0.1:8000/api/reservation/storeCart

login : http://127.0.0.1:8000/login

dashboard admin http://127.0.0.1:8000/admin

dashboard user http://127.0.0.1:8000/profile


Test Unitaire

Entity Article test
php vendor/bin/phpunit tests/Entity/ArticleTest.php

Entity category , Article , User ,  Reservation  , LineReservation
php vendor/bin/phpunit tests/Entity/ReservationTest.php

Entity User
php vendor/bin/phpunit tests/Entity/UserTest.php

Test controller Article
php vendor/bin/phpunit tests/Controller/ArticleControllerTest.php

Test controller reservation
php vendor/bin/phpunit tests/Controller/ReservationControllerTest.php








