<?php
namespace App\Service;


use phpDocumentor\Reflection\Types\Boolean;

interface ReservationServiceInterface
{
    /**
     * @param Boolean $status
     * @return void
     */
    public function getAllReservation($user = null,$status = false);

}