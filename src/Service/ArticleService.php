<?php

namespace App\Service;

use App\Repository\CategoryRepository;
use App\Repository\ArticleRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ArticleService implements ArticleServiceInterface
{
    /**
     * @var ArticleRepository
     */
    private ArticleRepository $articleRepository;
    private CategoryRepository $categoryRepository;
    private UrlGeneratorInterface $router;
    public function __construct(
        UrlGeneratorInterface $router,
        ArticleRepository $articleRepository,
        CategoryRepository $categoryRepository
    ) {
        $this->articleRepository = $articleRepository;
        $this->categoryRepository = $categoryRepository;
        $this->router = $router;
    }

    /**
     * @param array $base_url
     * @param boolean $status
     * @return array|void
     */
    public function getAllArticleByCategory(array $base_url, $status = false)
    {
        // TODO: Implement getArticleByCategory() method.
        $result = [];
        foreach ($this->categoryRepository->findAll(array('name' => 'ASC')) as $value) {
            if ($title = $this->articleRepository->findBy(['category' => $value->getId()])) {
                $result[$value->getName()] = [
                    'id' => $value->getId(),
                    'title' => $value->getName(),
                    'sub' => $value->getDescription(),
                    'detail' => []
                ];
                foreach ($title as $articleValue) {
                    $result[$value->getName()]["detail"][] = [
                        'id' => $articleValue->getId(),
                        'price' => $articleValue->getPrice(),
                        'name' => $articleValue->getName(),
                        'description' => $articleValue->getDescription(),
                        'slug' => $articleValue->getSlug(),
                        '_sub' => [
                            '_links' => $base_url["_http"] . '' . $this->router->generate('add_article'),
                            '_links_local' => $this->router->generate('add_article')
                        ]
                    ];
                }
            }
        }
        if ($status) {
            return $result;
        }
    }

    /**
     * @param array $base_url
     * @param int $category
     * @param String $slug
     * @param boolean $status
     * @return array|void
     */
    public function getArticleByCategoryFilterSlug(array $base_url, int $category, string $slug, $status = false)
    {
        $listArticles = [];
        $prevNext = [];
        if ($category && $slug) {
            $currentArticle = $this->articleRepository->findOneBy([
                'category' => (int)$category , 'slug' => (string)$slug
            ]);
            $currentAll = $this->articleRepository->findBy(['category' => (int)$category]);
            foreach ($currentAll as $key => $value) {
                if ($value->getId() === $currentArticle->getId()) {
                    continue;
                }
                $prevNext[] = [
                    '_link' => $base_url["_http"] . '/' . $base_url[
                        "_url"] . '/' . $value->getCategory()->getId() . '/' . $value->getSlug(),
                    '_route' => $this->router->generate('app_show_article', [
                        'id' => $value->getCategory()->getId(),'slug' => $value->getSlug()
                    ])
                ];
            }
            $listArticles['currentData'] = [
                'category' => $currentArticle->getCategory()->getName(),
                'descriptionCategory' => $currentArticle->getCategory()->getDescription(),
                'detail' => [
                    'name' => $currentArticle->getName(),
                    'price' => $currentArticle->getPrice(),
                    'description' => $currentArticle->getDescription(),
                    'slug' => $currentArticle->getSlug(),
                    '_links' => [
                        '_next' => next($prevNext),
                        '_prev' => prev($prevNext)
                    ]
                ]
            ];
        }
        if ($status) {
            return $listArticles;
        }
    }
}
