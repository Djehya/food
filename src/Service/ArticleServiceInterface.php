<?php

namespace App\Service;

use phpDocumentor\Reflection\Types\Boolean;

interface ArticleServiceInterface
{
    /**
     * @param array $base_url
     * @param boolean $status
     * @return mixed
     */
    public function getAllArticleByCategory(array $base_url, $status = false);

    /**
     * @param array $base_url
     * @param int $category
     * @param String $slug
     * @param boolean $status
     * @return mixed
     */
    public function getArticleByCategoryFilterSlug(array $base_url, int $category, string $slug, $status = false);
}
