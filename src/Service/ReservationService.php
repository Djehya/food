<?php

namespace App\Service;

use App\Repository\LineReservationRepository;
use App\Repository\ReservationRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ReservationService implements ReservationServiceInterface
{
    /**
     * @var array|false|string
     */
    private $lineReservationRepository;
    private ReservationRepository $reservationRepository;
    private $router;
    public function __construct(
        UrlGeneratorInterface $router,
        LineReservationRepository $lineReservationRepository,
        ReservationRepository $reservationRepository
    ) {
        $this->reservationRepository = $reservationRepository;
        $this->lineReservationRepository = $lineReservationRepository;
        $this->router = $router;
    }


    /**
     * @param boolean $status
     * @return void
     */
    public function getAllReservation($user = null, $status = false)
    {
        $result = [];
        foreach ( empty($user)  ? $this->reservationRepository->findAll(array('date' => 'ASC')) : $this->reservationRepository->findBy(array('userReservation' => $user)) as $value) {
            if ($lineReservations = $this->lineReservationRepository->findBy(['reservation' => $value->getId()])) {
                    $result[$value->getUserReservation()->getEmail()] = [
                        'id' => $value->getId(),
                        'name' => $value->getUserReservation()->getFirstname() . ' ' .
                            $value->getUserReservation()->getLastname(),
                        'phone' => $value->getUserReservation()->getPhone(),
                        'address' => $value->getUserReservation()->getAddress(),
                        'person' => $value->getPerson(),
                        'date' => $value->getDate() . ' à ' . $value->getTime(),
                        'status' => ($value->getStatus() === false ) ? 'sending' : ($value->getStatus() === true ? 'Accept' : 'refused'),
                        'list_reservation' => []
                    ];
                    foreach ($lineReservations as $key => $lineReservation) {
                        if ($lineReservation->getReservation()->getId() === $value->getId()) {
                            $result[$value->getUserReservation()->getEmail()]['list_reservation'][] = [
                            'category' => $lineReservation->getArticle()->getCategory()->getName(),
                            'article' => $lineReservation->getArticle()->getName(),
                            'price' => $lineReservation->getArticle()->getPrice(),
                            'number' => $lineReservation->getNumber(),
                            'total' => ($lineReservation->getArticle()->getPrice() * $lineReservation->getNumber()),
                            '_sub' => [
                                'delete' => '',
                                'accept' => ''
                            ]

                            ];
                        }
                    }
            }
        }
        if ($status) {
            return $result;
        }
    }

}
