<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=App\Repository\ArticleRepository::class)
 */

class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     *
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity=LineReservation::class, mappedBy="article", orphanRemoval=true)
     *
     */
    private $lineReservations;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $slug;

    public function __construct()
    {
        $this->lineReservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection<int, LineReservation>
     */
    public function getLineReservations(): Collection
    {
        return $this->lineReservations;
    }

    public function addLineReservation(LineReservation $lineReservation): self
    {
        if (!$this->lineReservations->contains($lineReservation)) {
            $this->lineReservations[] = $lineReservation;
            $lineReservation->setArticle($this);
        }

        return $this;
    }

    public function removeLineReservation(LineReservation $lineReservation): self
    {
        if ($this->lineReservations->removeElement($lineReservation)) {
            // set the owning side to null (unless already changed)
            if ($lineReservation->getArticle() === $this) {
                $lineReservation->setArticle(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
