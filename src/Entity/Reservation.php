<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PhpParser\Node\Scalar\String_;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ReservationRepository::class)
 */
class Reservation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $time;

    /**
     * @ORM\Column(type="integer")
     * 
     */
    private $person;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * 
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity=LineReservation::class, mappedBy="reservation", orphanRemoval=true)
     */
    private $lineReservations;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reservations",cascade="persist")
     * @ORM\JoinColumn(nullable=true)
     */
    private $userReservation;

    public function __construct()
    {
        $this->lineReservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(string $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getPerson(): ?int
    {
        return $this->person;
    }

    public function setPerson(int $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, LineReservation>
     */
    public function getLineReservations(): Collection
    {
        return $this->lineReservations;
    }

    public function addLineReservation(LineReservation $lineReservation): self
    {
        if (!$this->lineReservations->contains($lineReservation)) {
            $this->lineReservations[] = $lineReservation;
            $lineReservation->setReservation($this);
        }

        return $this;
    }

    public function removeLineReservation(LineReservation $lineReservation): self
    {
        if ($this->lineReservations->removeElement($lineReservation)) {
            // set the owning side to null (unless already changed)
            if ($lineReservation->getReservation() === $this) {
                $lineReservation->setReservation(null);
            }
        }

        return $this;
    }

    public function getUserReservation(): ?User
    {
        return $this->userReservation;
    }

    public function setUserReservation(?User $userReservation): self
    {
        $this->userReservation = $userReservation;

        return $this;
    }
}
