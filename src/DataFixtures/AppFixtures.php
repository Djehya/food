<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\User;
use App\Utils\StrUtil;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordHasher;

    /**
     * @param UserPasswordEncoderInterface $userPasswordHasher
     */
    public function __construct(UserPasswordEncoderInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        $manager->getConnection()->beginTransaction();
        try {
            // categories starters
            $starters = new Category();
            $starters->setName('Starters');
            $starters->setDescription('This is a Starters description');
            $manager->persist($starters);

            //article 1
            $article_s1 = new Article();
            $article_s1->setName('Grilled okra and tomatoes');
            $article_s1->setPrice(20);
            $article_s1->setDescription('this is a Grilled okra and tomatoes description');
            $article_s1->setCategory($starters);
            $article_s1->setSlug(StrUtil::slug($article_s1->getName()));
            $manager->persist($article_s1);

            //article 2
            $article_s2 = new Article();
            $article_s2->setName('Cucumber Salad');
            $article_s2->setPrice(12);
            $article_s2->setDescription('this is a Cucumber Salad description');
            $article_s2->setCategory($starters);
            $article_s2->setSlug(StrUtil::slug($article_s2->getName()));
            $manager->persist($article_s2);

            //article 3
            $article_s3 = new Article();
            $article_s3->setName('Basil Pancakes');
            $article_s3->setPrice(12);
            $article_s3->setDescription('this is a Basil Pancakes description');
            $article_s3->setCategory($starters);
            $article_s3->setSlug(StrUtil::slug($article_s3->getName()));
            $manager->persist($article_s3);


            // categories mains
            $mains = new Category();
            $mains->setName('Mains');
            $mains->setDescription('This is a Mains description');
            $manager->persist($mains);

            //article m_1
            $article_m1 = new Article();
            $article_m1->setName('Deep Sea Snow white Cod Fillet');
            $article_m1->setPrice(20);
            $article_m1->setDescription('this is a Deep Sea Snow white Cod Fillet description');
            $article_m1->setCategory($mains);
            $article_m1->setSlug(StrUtil::slug($article_m1->getName()));
            $manager->persist($article_m1);

            //article m_2
            $article_m2 = new Article();
            $article_m2->setName('Steack With Rosemary Butter');
            $article_m2->setPrice(22);
            $article_m2->setDescription('this is a Steack With Rosemary Butter description');
            $article_m2->setCategory($mains);
            $article_m2->setSlug(StrUtil::slug($article_m2->getName()));
            $manager->persist($article_m2);

            //article m_3
            $article_m3 = new Article();
            $article_m3->setName('Steack With Grilled kimchi');
            $article_m3->setPrice(20);
            $article_m3->setDescription('this is a Steack With Rosemary Butter description');
            $article_m3->setCategory($mains);
            $article_m3->setSlug(StrUtil::slug($article_m3->getName()));
            $manager->persist($article_m3);

            //categories pastries & drinks

            $pastriesdrinks = new Category();
            $pastriesdrinks->setName('Pastries & Drinks');
            $pastriesdrinks->setDescription('This is a Pastries & Drinks description');
            $manager->persist($pastriesdrinks);

            //article p_1
            $article_p1 = new Article();
            $article_p1->setName('Wine Paring');
            $article_p1->setPrice(158);
            $article_p1->setDescription('this is a Wine Paring description');
            $article_p1->setCategory($pastriesdrinks);
            $article_p1->setSlug(StrUtil::slug($article_p1->getName()));
            $manager->persist($article_p1);

            //article p_2
            $article_p2 = new Article();
            $article_p2->setName('Natural Wine Pairing');
            $article_p2->setPrice(22);
            $article_p2->setDescription('this is a Natural Wine Pairing description');
            $article_p2->setCategory($pastriesdrinks);
            $article_p2->setSlug(StrUtil::slug($article_p2->getName()));
            $manager->persist($article_p2);

            //article p_3
            $article_p3 = new Article();
            $article_p3->setName('Whisky Flyer');
            $article_p3->setPrice(90);
            $article_p3->setDescription('this is a Whisky Flyer description');
            $article_p3->setCategory($pastriesdrinks);
            $article_p3->setSlug(StrUtil::slug($article_p3->getName()));
            $manager->persist($article_p3);

            //admin
            $user = new User();
            $user->setEmail('admin@gmail.com');
            $user->setRoles(['ROLE_ADMIN']);
            $user->setFirstname('admin');
            $user->setLastname('admin');
            $user->setAddress('Tananarive');
            $user->setPhone('0345030878');
            $user->setPassword(
                $this->userPasswordHasher->encodePassword(
                    $user,
                    'admin'
                )
            );
            $manager->persist($user);

            $manager->flush();
            $manager->getConnection()->commit();
        } catch (Exception $e) {
            $manager->getConnection()->rollBack();
            throw $e;
        }
    }
}
