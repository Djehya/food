<?php
namespace App\Utils;

use Cocur\Slugify\Slugify;

class StrUtil
 {
   public static function slug(String $titre)
     {
         $slugify = new Slugify();
         return $slugify->slugify($titre);
     }
 }