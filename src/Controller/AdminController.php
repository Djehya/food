<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use App\Service\ReservationServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends AbstractController
{
    /**
     * @var ReservationRepository
     */
    private ReservationRepository $reservationRepository;
    private ReservationServiceInterface $reservationService;
    private EntityManagerInterface $entityManager;

    public function __construct(
        ReservationRepository $reservationRepository,
        ReservationServiceInterface $reservationService,
        EntityManagerInterface $entityManager
    ) {
        $this->reservationRepository = $reservationRepository;
        $this->reservationService = $reservationService;
        $this->entityManager = $entityManager;
    }

    /**
     * @return Response
     * @Route("/admin", name="admin")
     */
    public function indexAdmin() : Response
    {
        return $this->render('admin/admin.html.twig');
    }
    /**
     * all list article with reservation user
     * @Rest\Get("/api/admin/allReservation", name="dashboard")
     */
    public function index(): JsonResponse
    {
        if ($listReservation = $this->reservationService->getAllReservation(null,true)) {
            return  new JsonResponse([
                'data' => $listReservation
            ]);
        }
        return new JsonResponse([
            'message' => 'List empty'
        ]);
    }

    /**
     * delete reservtion with article lineReservation
     * @param Request $request
     * @return Response
     * @Rest\Post("/api/admin/reservation/delete", name="admin_delete")
     */
    public function deleteReservation(Request $request): Response
    {
        $req = $request->toArray();
        $id = $req["id"];
        if (!isset($id)) {
            return new JsonResponse([
                'error' => 400
            ]);
        }
        $reservation = $this->reservationRepository->find($id);
        $this->entityManager->remove($reservation);
        $this->entityManager->flush();
        return new JsonResponse([
                'success' => 200
        ]);
    }

    /**accept or refused reservation
     * @param Request $request
     * @return Response
     * @Rest\Put("/api/admin/reservation/accept", name="admin_accept")
     */
    public function acceptReservation(Request $request): Response
    {
        $req = $request->toArray();
        $id = $req["id"];
        $status = $req["status"];
        if (!isset($id)) {
            return new JsonResponse([
                'error' => 400
            ]);
        }
        $reservation = $this->reservationRepository->find($id);
        $reservation->setStatus($status ? true : null);
        $this->entityManager->flush();
        return new JsonResponse([
            'success' => 200
        ]);
    }
}
