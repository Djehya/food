<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * For now controls login and logout.
 *
 * Class SecurityController
 * @package App\Controller
 */

class SecurityController extends AbstractController
{
    /**
     * Log in user.
     *
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()){
            if (in_array('ROLE_ADMIN',$this->getUser()->getRoles())){
                return $this->render('admin/admin.html.twig');
            }elseif (in_array('ROLE_USER',$this->getUser()->getRoles())){
                return $this->render('Profile/profile.html.twig');
            }
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * Log out user, as said in throw LogicException, can be left blank and logout still works.
     *
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        
    }
}
