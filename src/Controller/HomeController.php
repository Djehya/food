<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\ReservationRepository;
use App\Service\ArticleServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Controller\Annotations as Rest;
use mysql_xdevapi\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class HomeController extends AbstractController
{
    const ROUTE_NAME = 'showItem';
    const ROUTE_NAME_ARTICLE = 'add_article';
    /**
     * @var ArticleServiceInterface
     */
    private ArticleServiceInterface $articleService;
    private ArticleRepository $article;
    private ReservationRepository $reservationRepository;
    private EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ReservationRepository $reservationRepository
     * @param ArticleServiceInterface $articleService
     * @param ArticleRepository $article
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ReservationRepository $reservationRepository,
        ArticleServiceInterface $articleService,
        ArticleRepository $article
    ) {
        $this->entityManager = $entityManager;
        $this->reservationRepository = $reservationRepository;
        $this->articleService = $articleService;
        $this->article = $article;
    }

    /**
     * All List Article by Category
     * @Rest\Get("/api/listArticle", name="app_home")
     */
    public function index(Request $request): Response
    {
        $base_url = [
            '_http' => $request->getSchemeAndHttpHost(),
            '_url' => self::ROUTE_NAME_ARTICLE
        ];
        if ($data = $this->articleService->getAllArticleByCategory($base_url, true)) {
            return  new JsonResponse([
                'status' => true,
                'data' => $data
            ]);
        }
            return  new JsonResponse([
                'status' => false,
                'message' => 'List not found!',
            ]);
    }

    /***
     * Show Article by category with next and previous
     * @Route("/show/{id}/{slug}", name="app_show_article_view", methods={"GET","POST"})
     */
    public function showArticleByCategory(Request $request, $id, $slug): Response
    {
        $base_url = [
            '_http' => $request->getSchemeAndHttpHost(),
            '_url' => self::ROUTE_NAME
        ];
        if ((int)$id && (string)$slug) {
            $data = $this->articleService->getArticleByCategoryFilterSlug($base_url, $id, $slug, true);
            return new JsonResponse([
                'status' => 200,
                'data' => $data
            ]);
        }
        return new JsonResponse(false);
    }

    /**
     * Show Article by category with next and previous [Twig call Vue]
     * @Route ("/showItem/{id}/{slug}", name="app_show_article" , methods={"GET","PUT"})
     */
    public function showArticle(Environment $twig,Request $request, $id, $slug): Response
    {
        if ($request->isMethod('PUT')){
            $base_url = [
                '_http' => $request->getSchemeAndHttpHost(),
                '_url' => self::ROUTE_NAME
            ];
            if ((int)$id && (string)$slug) {
                $data = $this->articleService->getArticleByCategoryFilterSlug($base_url, $id, $slug, true);
                return new JsonResponse([
                    'status' => 200,
                    'data' => $data
                ]);
            }
        }
        return new Response($twig->render('/home/show.html.twig', []));
    }

    /**
     * reset All cart in session
     * @param Request $request
     * @return Response
     * @Rest\Delete("/api/cart/rest", name="add_reservation_reset")
     */
    public function resetCart(Request $request):Response
    {
        $request->getSession()->remove('user.cart');
        return  new JsonResponse(true);
    }
}
