<?php

/**
 * Controller for home page.
 */

namespace App\Controller;


use App\Entity\CMSBanner;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Controller for homepage.
 *
 * Class HomeController
 * @package App\Controller
 */

class DefaultController extends AbstractController
{

    /**
     * Render homepage, get featured products and pass them to view.
     *
     * @Route("/", name="homepage")
     *
     * @param Environment $twig
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */

    public function homePage(Environment $twig) : Response {
        if ($this->getUser()){
            if (in_array('ROLE_ADMIN',$this->getUser()->getRoles())){
                return $this->redirectToRoute('admin');
            }elseif (in_array('ROLE_USER',$this->getUser()->getRoles())){
                return $this->redirectToRoute('profile');
            }
        }
        return new Response($twig->render('/home/home.html.twig', []));
    }



}