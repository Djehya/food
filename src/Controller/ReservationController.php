<?php

namespace App\Controller;

use App\Repository\ReservationRepository;
use App\Service\ReservationServiceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_USER")
 */
class ReservationController extends AbstractController
{
    /**
     * @var ReservationServiceInterface
     */
    private ReservationServiceInterface $reservationService;

    public function __construct(
        ReservationServiceInterface $reservationService
    ) {
        $this->reservationService = $reservationService;
    }

    /**
     * profile user
     * @Route("/profile", name="profile")
     */
    public function index(): Response
    {
        return $this->render('profile/profile.html.twig');
    }

    /**
     * profile user and list reservation
     * @Rest\Get("/api/profile/allReservation",name="user_allReservation")
     */
    public function listReservation(): JsonResponse
    {
        $user = $this->getUser();
        if ($listReservation = $this->reservationService->getAllReservation($user,true)) {
            return  new JsonResponse([
                'data' => $listReservation
            ]);
        }
        return new JsonResponse([
            'message' => 'List empty'
        ]);
    }
}
