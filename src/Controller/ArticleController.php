<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\LineReservation;
use App\Entity\Reservation;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Repository\ReservationRepository;
use App\Security\LoginFormAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class ArticleController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private ReservationRepository $reservationRepository;
    private ArticleRepository $articleRepository;
    private UserPasswordEncoderInterface $userPassword;

    /**
     * @param UserPasswordEncoderInterface $userPassword
     * @param ReservationRepository $reservationRepository
     * @param EntityManagerInterface $entityManager
     * @param ArticleRepository $articleRepository
     */
    public function __construct(
        UserPasswordEncoderInterface $userPassword,
        ReservationRepository $reservationRepository,
        EntityManagerInterface $entityManager,
        ArticleRepository $articleRepository
    ) {
        $this->userPassword = $userPassword;
        $this->reservationRepository = $reservationRepository;
        $this->entityManager = $entityManager;
        $this->articleRepository = $articleRepository;
    }

    /**
     * add or remove article in session
     * @Rest\Post("/api/article/add", name="add_article")
     * @return JsonResponse
     *  @Rest\View()
     */
    public function addArticleToCart(Request $request): Response
    {
        $req = $request->toArray();
        $id = $req["id"];
        $status = $req["status"];
        $article = $this->articleRepository->find($id);
        if (!$article) {
            return new JsonResponse([
                'error' => 400,
                'message' => $article->getId() . ' Not found'
            ]);
        }
        $cart = $request->getSession()->get("user.cart", ["total" => 0, "menus" => []]);
        $cart["menus"][$article->getId()]["details"] = [
            'id' => $article->getId(),
            'name' => $article->getName(),
            'price' => $article->getPrice(),
            'category'=> $article->getCategory()->getName()
        ];
        if (false === $status){
            $cart["total"] += $article->getPrice();
            $cart["menus"][$article->getId()]["qte"] = empty(
            $cart["menus"][$article->getId()]["qte"]
            ) ? 1 : $cart["menus"][$article->getId()]["qte"] + 1;
        }else{
            if($cart["menus"][$article->getId()]["qte"] >= 1){
                $cart["total"] -= $article->getPrice();
                $cart["menus"][$article->getId()]["qte"] = $cart["menus"][$article->getId()]["qte"] - 1;
            }
        }
        $request->getSession()->set("user.cart", $cart);
        return new JsonResponse([
            'status' => 200,
            'data' => $cart
        ]);
    }

    /**
     * delete all session article
     * @param Request $request
     * @return Response
     * @Rest\Delete("/api/article/remove", name="remove_article")
     */
    public function removeCartArticle(Request $request): Response
    {
        // remove article in session
        $req = $request->request->all();
        $id = $req["id"];
        $article = $this->articleRepository->findOneBy(['id' =>$id]);
        if (!$article) {
            return new JsonResponse([
                'error' => 400,
                'message' => $id . ' Not found'
            ]);
        }
        $cart = $request->getSession()->get("user.cart", []);

        if (empty($cart)) {
            return new JsonResponse([
                'message' => 'cart is empty',
                'code' => 400
            ]);
        }
        $article = $cart["menus"][$article->getId()]["details"];
        $qte = $cart["menus"][$article["id"]]["qte"];
        unset($cart["menus"][$article["id"]]);
        $cart["total"] = $cart["total"] - ($qte * $article["price"]);
        $request->getSession()->set("user.cart", $cart);
        return new JsonResponse([
            'data' => $cart
        ]);
    }

    /**
     * Show all cart Session
     * @Rest\Get("/api/article/viewListCartArticle", name="show_list_article")
     * @throws Exception
     */
    public function showAllArticle(Request $request): Response {
        $cart = $request->getSession()->get("user.cart", ["total" => 0, "menus" => []]);
        return new JsonResponse($cart);
    }

    /**
     * add reservation book now
     * @param Request $request
     * @return Response
     * @Rest\Post("/api/reservation/add", name="add_reservation_api")
     * @throws ORMException
     */
    public function addReservation(Request $request):Response
    {
        $data = $request->toArray();
        return $this->reservationRepository->addRequestData($data, true);
    }

    /**
     * Store all Session cart to database ,Entity User , Reservation , lineReservation
     * @param Request $request
     * @return Response
     * @throws Exception
     * @Rest\Post("/api/reservation/storeCart", name="store_reservationCart")
     */
    public function addReservationFormUser(Request $request): Response
    {
        $data = $request->toArray();
        $reservation = $this->reservationRepository->find($data["id"]);
        $this->entityManager->beginTransaction();
            try {
                $user = new User();
                $user->setEmail($data["email"]);
                $user->setRoles(['ROLE_USER']);
                $user->setLastname($data["lastname"]);
                $user->setAddress($data["address"]);
                $user->setPhone($data["phone"]);
                $user->setFirstname($data["firstname"]);
                $user->setPassword(
                    $this->userPassword->encodePassword(
                        $user,
                        $data["password"]
                    )
                );
                $reservation->setUserReservation($user);
                $this->entityManager->persist($reservation);
                $cart = $request->getSession()->get("user.cart", ["total" => 0, "menus" => []]);


                $totals = 0;
                $i = 0;
                foreach ($cart["menus"] as $value) {
                    $article = $this->articleRepository->find($value["details"]["id"]);
                    $totals = ($value["details"]["price"] * $value["qte"]) + $totals;
                    $line[$i] = new LineReservation();
                    $line[$i]->setNumber($value["qte"]);
                    $line[$i]->setReservation($reservation);
                    $line[$i]->setArticle($article);
                    $this->entityManager->merge($line[$i]);
                    $i++;

                }
                $this->entityManager->flush();
                $this->entityManager->commit();
                $request->getSession()->remove('user.cart');
                return new JsonResponse([
                    'code' => 200
                ]);
            } catch (Exception $e) {
                $this->entityManager->rollBack();
                throw $e;
            }
            return true;
    }
}
