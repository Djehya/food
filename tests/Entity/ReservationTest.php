<?php

use App\Entity\Article;
use App\Entity\User;
use App\Entity\Reservation;
use App\Entity\LineReservation;
use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class ReservationTest extends TestCase {
public function testReservation() {
    $categoryCart = new Category();
    $categoryCart->setName('testCategoryCart');
    $categoryCart->setDescription('descriptionCategory');

    $name = "Grilled okra and tomatoes";

    $article = new Article();
    $article->setName($name);
    $article->setCategory($categoryCart);
    $article->setDescription('description');
    $article->setPrice(12);
    $article->setSlug('test-slug');

    $userName = "Djehya";

    $user = new User();
    $user->setFirstname('testFirstname');
    $user->setPhone('03562544');
    $user->setAddress('tana');
    $user->setEmail('djehya@gmail.com');
    $user->setPassword('123456');
    $user->setLastname($userName);
    $person = 2;
    $reservation = new Reservation();
    $reservation->setUserReservation($user);
    $reservation->setPerson($person);
    $reservation->setTime('12:04');
    $reservation->setDate('12/04/2021');
    $reservation->setStatus(false);

    $number = 1;
    $line = new LineReservation();
    $line->getArticle($article);
    $line->setReservation($reservation);
    $line->setNumber($number);
    $this->assertEquals($number, $line->getNumber());
    $this->assertEquals($name, $article->getName());
    $this->assertEquals($userName, $user->getLastname());
    $this->assertEquals($person, $reservation->getPerson());
}
}