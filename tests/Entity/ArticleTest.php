<?php


namespace App\Tests\Entity;


use App\Entity\Article;
use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase {
    public function testArticleCategory() {
        $nameCategory = 'testCategory';
        $category = new Category();
        $category->setName($nameCategory);
        $category->setDescription('descriptionCategory');
        $name = "Grilled okra and tomatoes";
        $article = new Article();
        $article->setName($name);
        $article->setCategory($category);
        $article->setDescription('description');
        $article->setPrice(12);
        $article->setSlug('test-slug');
        $this->assertEquals($name, $article->getName());
        $this->assertEquals($nameCategory, $category->getName());
    }
}