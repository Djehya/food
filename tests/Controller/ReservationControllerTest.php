<?php

namespace App\Test\Controller;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ReservationControllerTest extends WebTestCase
{
    protected function setup(): void
    {
        parent::setup();
        $this->client = static::createClient();
    }

    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url)
    {
        $this->client->request('GET', $url);
        $this->assertResponseIsSuccessful();
    }

    public function urlProvider()
    {
        yield ['/api/profile/allReservation'];
    }
}
